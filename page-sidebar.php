<?php
/*
*Template Name: Sidebar Page
 * @package regiment-secondhero
 */


get_header(); ?>
<div id="header-bump"></div>


		<div id="page" class="hfeed site">

			<?php if ( get_field( 'page_secondhero_image' ) ): ?>
<div id="secondhero-top">		
<img src="<?php echo esc_url( get_field( 'page_secondhero_image' ) ); ?>"/>	

<?php if( get_field('show_title') == 'show' ): ?>
<h1><?php the_title(); ?></h1>
<?php endif; ?><!-- the select clause -->

</div>
<?php endif; ?>	

<?php if (! get_field( 'page_secondhero_image' ) ): ?>
<div id="big-header-bump"></div>
<?php endif; ?>	
	<div id="content" class="site-content" >
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
	<?php if ( !get_field( 'page_secondhero_image' ) ): ?>
	<h1><?php the_title(); ?></h1>	
	<?php endif; ?>	
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php the_content(); ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'regiment-secondhero' ),
				'after'  => '</div>',
			) );
		?>
	</div><!-- .entry-content -->
	<?php edit_post_link( __( 'Edit', 'regiment-secondhero' ), '<footer class="entry-footer"><span class="edit-link">', '</span></footer>' ); ?>
</article><!-- #post-## -->



			<?php endwhile; // end of the loop. ?>

		</main><!-- #main -->
	</div><!-- #primary -->

	<div id="secondary" class="widget-area front-book-array" role="complementary">




<?php if ( ! dynamic_sidebar( 'sidebar-3' ) ) : ?>

<?php endif; // end sidebar widget area ?>






	</div><!-- #secondary -->
	<div class="clear" style="height:2em;"></div>

<?php get_footer(); ?>
