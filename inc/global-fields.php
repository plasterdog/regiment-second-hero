<?php


add_action('admin_menu', 'add_gcf_interface');

function add_gcf_interface() {
  add_theme_page('Global Custom Fields', 'Global Custom Fields', 'manage_options', 'functions', 'editglobalcustomfields');
}

function editglobalcustomfields() {
  ?>
  <div class='wrap'>
  
  <hr/>
  <h2>Global Custom Fields</h2>
  <p>Questions? contact Jeff McNear <a href="mailto:jeff@plasterdog.com?subject=Global Custom Fields mini plugin">email me</a></p>
  
<hr style="clear:both;"/>
  <form method="post" action="options.php">
  <?php wp_nonce_field('update-options') ?>
<div style="float:left; width:48%;">
<h2>SOCIAL MEDIA:</h2>
<p>Enter into these fields the full URL (including the leading http:// ) for each relevant social media outlet, your theme is configured to show a linked icon
  for any populated field</p>

<hr/>



            <p><strong>FACEBOOK TARGET URL:</strong><br />
            <input type="text" name="pdog_facebook" size="75" value="<?php echo get_option('pdog_facebook'); ?>" /></p>

            <p><strong>LINKEDIN TARGET URL:</strong><br />
            <input type="text" name="pdog_linkedin" size="75" value="<?php echo get_option('pdog_linkedin'); ?>" /></p>
            
            <p><strong>TWITTER TARGET URL:</strong><br />
            <input type="text" name="pdog_twitter" size="75" value="<?php echo get_option('pdog_twitter'); ?>" /></p>      

            <p><strong>INSTAGRAM TARGET URL:</strong><br />
            <input type="text" name="pdog_instagram" size="75" value="<?php echo get_option('pdog_instagram'); ?>" /></p>

            <p><strong>YOUTUBE TARGET URL:</strong><br />
            <input type="text" name="pdog_youtube" size="75" value="<?php echo get_option('pdog_youtube'); ?>" /></p>

           <p><strong>VIMEO TARGET URL:</strong><br />
            <input type="text" name="pdog_vimeo" size="75" value="<?php echo get_option('pdog_vimeo'); ?>" /></p>
            

<h2> GOOGLE ANALYTICS ID</h2>  
<p>The tracking ID is a string like UA-000000-2. It must be included in your tracking code to tell Analytics which account and property to send data to.</p>
           <p><strong>ANALYTICS ID:</strong><br />
            <input type="text" name="pdog_analytics" size="75" value="<?php echo get_option('pdog_analytics'); ?>" /></p>

           <p><strong>ATTRIBUTION LABEL:</strong><br />
            <input type="text" name="pdog_attribute" size="75" value="<?php echo get_option('pdog_attribute'); ?>" /></p>

            <p><strong>ATTRIBUTION TARGET:</strong><br />
            <input type="text" name="pdog_target" size="75" value="<?php echo get_option('pdog_target'); ?>" /></p>           

</div>

<div style="float:right; width:48%;">
<h2>CONTACT INFORMATION:</h2>
<p>These fields will show any inserted text (including punctuation)</p><br/>
<hr/>
           <p><strong>PRIMARY ADDRESS:</strong><br />
            <input type="text" name="pdog_address" size="75" value="<?php echo get_option('pdog_address'); ?>" /></p>

            <p><strong>SECONDARY ADDRESS:</strong><br />
            <input type="text" name="pdog_address2" size="75" value="<?php echo get_option('pdog_address2'); ?>" /></p>           

            <p><strong>PHONE NUMBER:</strong><br />
            <input type="text" name="pdog_phone" size="75" value="<?php echo get_option('pdog_phone'); ?>" /></p>
            
            <p><strong>SECONDARY NUMBER:</strong><br />
            <input type="text" name="pdog_phone2" size="75" value="<?php echo get_option('pdog_phone2'); ?>" /></p>

            <p><strong>EMAIL ADDRESS:</strong><br />
            <input type="text" name="pdog_email" size="75" value="<?php echo get_option('pdog_email'); ?>" /></p>

           

<hr/>

 </div>          
<hr style="clear:both;"/>

  <p><input type="submit" name="Submit" value="Update Options" /></p>

  <input type="hidden" name="action" value="update" />
  <input type="hidden" name="page_options" value="pdog_facebook, pdog_linkedin, pdog_twitter, pdog_instagram, pdog_youtube, pdog_vimeo,  pdog_phone, pdog_phone2, pdog_address, pdog_address2, pdog_email, pdog_analytics,pdog_attribute,pdog_target,"/>

  </form>
  </div>
  <?php
}
//believe it or not closing the php tag will trow a plugin error here
