Plasterdog Customizer theme
===

CUSTOMIZER CONTROLS: This theme has a significantly expanded customizer which includes controls over most color selections

    header text color
    background color
    link color
    link hover color
    navigation link color
    navigation link hover color
    social media link color
    social media link hover color
    headings color
    body text color
    header background color
    footer background color
    footer text color
    border color

PAGE TEMPLATES: All page templates and posts can show a custom background image.

    → Use the Archive Page template to show customized arrays of category defined posts.
    → The Landing Page template allows for a three linked feature array with a full width section which can be positioned either above or below the feature elements.
    → Split page and Full page templates are also available.

LINKS AND CONTACT INFORMATION: To set Social Media links, Contact Information and connection to Google Analytics go to
→ appearance
→ Global Custom Fields

STREAMLINED ADMIN INTERFACE: Clutter has been reduced in the dashboard - in particular the widgets region where the seldom used widgets of dubious importance have been eliminated:


