<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package regiment-secondhero
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>

<?php if(get_option('pdog_analytics') && get_option('pdog_analytics') != '') {?>
		<!-- Global Site Tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=<?php echo get_option('pdog_analytics') ?>"></script>
		<script>
		  window.dataLayer = window.dataLayer || [];
		  function gtag(){dataLayer.push(arguments);}
		  gtag('js', new Date());

		  gtag('config', '<?php echo get_option('pdog_analytics') ?>');
		</script>
<?php } ?>
	
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">

<!--[if lt IE 9]>
<script>
  var e = ("abbr,article,aside,audio,canvas,datalist,details," +
    "figure,footer,header,hgroup,mark,menu,meter,nav,output," +
    "progress,section,time,video,main").split(',');
  for (var i = 0; i < e.length; i++) {
    document.createElement(e[i]);
  }
</script>
<![endif]-->
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?> >
<?php if ( get_theme_mod( 'themeslug_logo' ) ) : ?>	
<div id="full-top" style="background-image:url(<?php echo esc_url( get_theme_mod( 'themeslug_logo' ) ); ?>);  background-repeat:no-repeat;  background-position:center top; background-size:cover;">
<?php else : ?>
<div id="full-top">
<?php endif; ?>	
	<div id="upper-band">



<div class="masthead-holder">
	<header id="masthead" class="site-header" role="banner">
		<div class="site-branding">


<!-- inserts the header image-->
	<div id="left-head-1">

						<?php $header_image = get_header_image();
						if ( ! empty( $header_image ) ) { ?>
						<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
						<img src="<?php header_image(); ?>" width="<?php echo get_custom_header()->width; ?>" height="<?php echo get_custom_header()->height; ?>" alt="" />
						</a>
						<?php } // if ( ! empty( $header_image ) ) ?>

	</div><!-- ends left head 1 -->

<div id="right-head">

<div class="navigation-section">
	<nav id="site-navigation" class="main-navigation" role="navigation">
		<!-- THIS IS WHERE YOU SET THE MENU TOGGLE TEXT -->	
			<h1 class="menu-toggle"><?php _e( 'menu', 'regiment-secondhero' ); ?></h1>
			<a class="skip-link screen-reader-text" href="#content"><?php _e( 'Skip to content', 'regiment-secondhero' ); ?></a>
			<?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
		</nav><!-- #site-navigation -->
</div><!-- ends navigation section -->		



		</div><!--ends right head -->			





				</div><!-- ends site branding -->
	</header><!-- ends masthead -->
</div><!-- ends masthead holder -->


</div><!-- ends upper band-->
	</header><!-- #masthead -->

</div> <!-- ends full top -->


