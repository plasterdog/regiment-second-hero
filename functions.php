<?php
/**
 * regiment_secondhero functions and definitions
 *
 * @package regiment-secondhero
 */
require_once( __DIR__ . '/inc/customizer-styles.php');
require_once( __DIR__ . '/inc/simplify-profiles.php');
require_once( __DIR__ . '/inc/widget-styling.php');
require_once( __DIR__ . '/inc/backend-experience.php');
require_once( __DIR__ . '/inc/global-fields.php');

//require_once( __DIR__ . '/inc/required-plugins.php');
/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}

if ( ! function_exists( 'regiment_secondhero_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function regiment_secondhero_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on regiment_secondhero, use a find and replace
	 * to change 'regiment-secondhero' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'regiment-secondhero', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );
	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	//add_theme_support( 'post-thumbnails' );
	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'regiment-secondhero' ),
		'footer' => __( 'Footer Menu', 'regiment-secondhero' ),
	) );
	// Setup the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'regiment_secondhero_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );


	// Enable support for HTML5 markup.
	add_theme_support( 'html5', array(
		'comment-list',
		'search-form',
		'comment-form',
		'gallery',
	) );
}
endif; // regiment_secondhero_setup
add_action( 'after_setup_theme', 'regiment_secondhero_setup' );

//https://wordpress.stackexchange.com/questions/250349/how-to-remove-menus-section-from-wordpress-theme-customizer

function mytheme_customize_register( $wp_customize ) {
  //All our sections, settings, and controls will be added here

  //$wp_customize->remove_section( 'title_tagline');
  $wp_customize->remove_panel( 'nav_menus');
  $wp_customize->remove_panel( 'widgets');
  //$wp_customize->remove_section( 'static_front_page');
}
add_action( 'customize_register', 'mytheme_customize_register',50 );




/**
 * Register widgetized area and update sidebar with default widgets.
 */
function regiment_secondhero_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Archive & Post Sidebar', 'regiment-secondhero' ),
		'id'            => 'sidebar-1',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>'
	) );

        register_sidebar( array(
        'name'          => __( 'Regular Page Sidebar', 'regiment-secondhero' ),
        'id'            => 'sidebar-3',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h1 class="widget-title">',
        'after_title'   => '</h1>'
    ) );

        register_sidebar( array(
        'name'          => __( 'Home Footer Region Widget', 'regiment-secondhero' ),
        'id'            => 'sidebar-4',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
        'before_title'  => '<h1 class="widget-title">',
        'after_title'   => '</h1>'
    ) );


 }
add_action( 'widgets_init', 'regiment_secondhero_widgets_init' );

/**
 * Enqueue scripts and styles.
 */



function regiment_secondhero_scripts() {

	wp_enqueue_style( 'reset-styles', get_template_directory_uri() . '/css/reset-styles.css',false,'1.1','all');

	wp_enqueue_style( 'regiment_secondhero-style', get_stylesheet_uri() );

	wp_enqueue_script( 'regiment_secondhero-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );

	wp_enqueue_script( 'regiment_secondhero-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'regiment_secondhero_scripts' );

/**
 * Implement the Custom Header feature. - JMC ACTIVATED
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';
//JMC => additional image size
function pw_add_image_sizes() {
    add_image_size( 'large-thumb', 300, 300, true );

}
add_action( 'init', 'pw_add_image_sizes' );
 
function pw_show_image_sizes($sizes) {
    $sizes['large-thumb'] = __( 'Custom Thumb', 'regiment-secondhero' );

 
    return $sizes;
}
add_filter('image_size_names_choose', 'pw_show_image_sizes');



 /* JMC --* Enable support for Post Thumbnails	 */
	add_theme_support( 'post-thumbnails' );

/* JMC because it is required */
	add_theme_support( 'title-tag' );

//JMC remove anchor link from more tag

function remove_more_anchor($link) {
     $offset = strpos($link, '#more-');
     if ($offset) {
          $end = strpos($link, '"',$offset);
     }
     if ($end) {
          $link = substr_replace($link, '', $offset, $end-$offset);
     }
     return $link;
}
add_filter('the_content_more_link', 'remove_more_anchor');

//JMC-https://carriedils.com/add-editor-style/ <= will apply the stylesheet to the editor view

add_action( 'init', 'cd_add_editor_styles' );
/**
 * Apply theme's stylesheet to the visual editor.
 *
 * @uses add_editor_style() Links a stylesheet to visual editor
 * @uses get_stylesheet_uri() Returns URI of theme stylesheet
 */
function cd_add_editor_styles() {
 add_editor_style( get_stylesheet_uri() );
}

/** https://gist.github.com/scottparry/be66973877ea42b8531a01f5a23580b9
  * Enqueue custom fonts using protocol relative URL.
  *
  * Syntax: wp_enqueue_style( $handle, $src, $deps, $ver, $media );
  * Ensure $handle is unique to prevent conflicts with plugins
  *
  * Note(s): The pipe (|) operator is used to load multiple typefaces in a single call. We also only load the weights we want   * by comma seperating them, instead of loading every available weight.
  */
function theme_prefix_fonts() 
{
    wp_enqueue_style( 'theme-prefix-fonts', "//fonts.googleapis.com/css?family=Oswald:400,600 | //fonts.googleapis.com/css?family=Roboto+Condensed:400,400i,700,700i | //fonts.googleapis.com/css?family=Roboto:400,400i,500,500i,700,700i", '', '1.0.0', 'screen' );
}
add_action( 'wp_enqueue_scripts', 'theme_prefix_fonts' );

//control width of Gutenberg editor https://github.com/WordPress/gutenberg/issues/6064
function PDOG_admin_css() {
echo '<style type="text/css">
.wp-block { max-width: 1200px; }
</style>';
}
add_action('admin_head', 'PDOG_admin_css');

// REGISTERING THE BLOCK 

add_action('acf/init', 'my_acf_init');
function my_acf_init() {
	
	// check function exists
	if( function_exists('acf_register_block') ) {
		
		// #1 register a bio block
		acf_register_block(array(
			'name'		=> 'bio',
			'title'		=> __('Bio'),
			'description'	=> __('A custom bio block.'),
			'render_template'	=> 'inc/blocks/bio.php',
			'category'		=> 'formatting',
			'icon'		=> 'businessman',
			// icon is a dashicon https://developer.wordpress.org/resource/dashicons/
			'keywords'	=> array( 'bio', 'plasterdog', 'custom' ),
			'enqueue_style'	=> get_stylesheet_directory_uri() . '/inc/blocks/biostyle.css',
		));

		// #2 register a feature quote block
		acf_register_block(array(
			'name'		=> 'featurequote',
			'title'		=> __('Feature Quote'),
			'description'	=> __('A custom feature quote block.'),
			'render_template'	=> 'inc/blocks/quote.php',
			'category'		=> 'formatting',
			'icon'		=> 'format-quote',
			// icon is a dashicon https://developer.wordpress.org/resource/dashicons/
			'keywords'	=> array( 'feature', 'quote', 'plasterdog', 'custom' ),
			'enqueue_style'	=> get_stylesheet_directory_uri() . '/inc/blocks/quotestyle.css',
		));	

		// #3 register aa accordion block
		acf_register_block(array(
			'name'		=> 'accordion',
			'title'		=> __('Accordion'),
			'description'	=> __('A custom call to action block.'),
			'render_template'	=> 'inc/blocks/accordion.php',
			'category'		=> 'formatting',
			'icon'		=> 'welcome-learn-more',
			// icon is a dashicon https://developer.wordpress.org/resource/dashicons/
			'keywords'	=> array( 'cta','call to action', 'plasterdog', 'custom' ),
			'enqueue_style'	=> get_stylesheet_directory_uri() . '/inc/blocks/accordion.css',
			'enqueue_script' => get_template_directory_uri() . '/inc/blocks/accordion.js',
		));		


	}
}

// RENDERING THE BLOCK

function my_acf_block_render_callback( $block ) {
	
	// convert name ("acf/bio") into path friendly slug ("bio")
	$slug = str_replace('acf/', '', $block['name']);
	
	// include a template part from within the "template-parts/block" folder for the block
	if( file_exists( get_theme_file_path("/inc/blocks/bio-{$slug}.php") ) ) {
		include( get_theme_file_path("/inc/blocks/bio-{$slug}.php") );
	}

	// include a template part from within the "template-parts/block" folder for the block
	if( file_exists( get_theme_file_path("/inc/blocks/quote-{$slug}.php") ) ) {
		include( get_theme_file_path("/inc/blocks/quote-{$slug}.php") );
	}

	// include a template part from within the "template-parts/block" folder for the block
	if( file_exists( get_theme_file_path("/inc/blocks/accordion-{$slug}.php") ) ) {
		include( get_theme_file_path("/inc/blocks/accordion-{$slug}.php") );
	}

}
