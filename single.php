<?php
/**
 * The Template for displaying all single posts.
 *
 * @package regiment-secondhero
 */

get_header(); ?>

    <div id="page" class="hfeed site">

<?php if ( get_field( 'page_secondhero_image' ) ): ?>
<div id="secondhero-top">   
<img src="<?php echo esc_url( get_field( 'page_secondhero_image' ) ); ?>"/> 

<?php if( get_field('show_title') == 'show' ): ?>
<h1><?php the_title(); ?></h1>
<?php endif; ?><!-- the select clause -->

</div>
<?php endif; ?> 



  <div id="content" class="site-content" >
  <div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">

    <?php while ( have_posts() ) : the_post(); ?>

  <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
  <header class="entry-header">
  <div class="entry-meta"></div><!-- .entry-meta -->
  </header><!-- .entry-header -->
  <div class="entry-content">
  <header class="entry-header">
  <?php if ( !get_field( 'page_secondhero_image' ) ): ?>
  <h1><?php the_title(); ?></h1>  
  <?php endif; ?> 
  </header><!-- .entry-header -->

    <?php the_content(); ?>

<?php the_tags( __( 'Tags: ', 'regiment-secondhero' ), ' ', '' ); ?>
  </div><!-- .entry-content -->

  <footer class="entry-footer">
    

    <?php edit_post_link( __( 'Edit', 'regiment-secondhero' ), '<span class="edit-link">', '</span>' ); ?>
  </footer><!-- .entry-footer -->
</article><!-- #post-## -->

        <div class="clear"><!-- variation from default nav which restricts navigation within category -->
<div class="left-split-nav"><?php //previous_post_link('%link', '&larr; %title', TRUE) ?></div>
<div class="right-split-nav"><?php //next_post_link('%link', '%title &rarr;', TRUE) ?></div>
</div>

      <?php
        // If comments are open or we have at least one comment, load up the comment template
        if ( comments_open() || '0' != get_comments_number() ) :
          comments_template();
        endif;
      ?>

    <?php endwhile; // end of the loop. ?>

    </main><!-- #main -->
  </div><!-- #primary -->

  <div id="secondary" class="widget-area" role="complementary">

     <?php if ( ! dynamic_sidebar( 'sidebar-1' ) ) : ?>


    <?php endif; // end sidebar widget area ?>
  </div><!-- #secondary -->
  <div class="clear" style="height:2em;"></div>

<?php get_footer(); ?>