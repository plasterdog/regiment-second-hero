<?php
/**
 * The template for displaying Archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package regiment-secondhero
 */

get_header(); ?>





		<div id="page" class="hfeed site">
	<div id="content" class="site-content" >
	<section id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
<header class="page-header">
				<h1 class="page-title">
					<?php
						if ( is_category() ) :
							single_cat_title();

						elseif ( is_tag() ) :
							single_tag_title();

						elseif ( is_author() ) :
							printf( __( 'Author: %s', 'regiment-secondhero' ), '<span class="vcard">' . get_the_author() . '</span>' );

						elseif ( is_day() ) :
							printf( __( 'Day: %s', 'regiment-secondhero' ), '<span>' . get_the_date() . '</span>' );

						elseif ( is_month() ) :
							printf( __( 'Month: %s', 'regiment-secondhero' ), '<span>' . get_the_date( _x( 'F Y', 'monthly archives date format', 'regiment-secondhero' ) ) . '</span>' );

						elseif ( is_year() ) :
							printf( __( 'Year: %s', 'regiment-secondhero' ), '<span>' . get_the_date( _x( 'Y', 'yearly archives date format', 'regiment-secondhero' ) ) . '</span>' );

						elseif ( is_tax( 'post_format', 'post-format-aside' ) ) :
							_e( 'Asides', 'regiment-secondhero' );

						elseif ( is_tax( 'post_format', 'post-format-gallery' ) ) :
							_e( 'Galleries', 'regiment-secondhero');

						elseif ( is_tax( 'post_format', 'post-format-image' ) ) :
							_e( 'Images', 'regiment-secondhero');

						elseif ( is_tax( 'post_format', 'post-format-video' ) ) :
							_e( 'Videos', 'regiment-secondhero' );

						elseif ( is_tax( 'post_format', 'post-format-quote' ) ) :
							_e( 'Quotes', 'regiment-secondhero' );

						elseif ( is_tax( 'post_format', 'post-format-link' ) ) :
							_e( 'Links', 'regiment-secondhero' );

						elseif ( is_tax( 'post_format', 'post-format-status' ) ) :
							_e( 'Statuses', 'regiment-secondhero' );

						elseif ( is_tax( 'post_format', 'post-format-audio' ) ) :
							_e( 'Audios', 'regiment-secondhero' );

						elseif ( is_tax( 'post_format', 'post-format-chat' ) ) :
							_e( 'Chats', 'regiment-secondhero' );

						else :
							_e( 'Archives', 'regiment-secondhero' );

						endif;
					?>
				</h1>
				<?php
					// Show an optional term description.
					$term_description = term_description();
					if ( ! empty( $term_description ) ) :
						printf( '<div class="taxonomy-description">%s</div>', $term_description );?>
			<?php endif;?>
			<hr/>
		<?php if ( have_posts() ) : ?>

			<?php /* Start the Loop */ ?>
			<ul class="just-archive-array">
			<?php global $query_string;
query_posts( $query_string.'&order=DESC' );	?>
			<?php while ( have_posts() ) : the_post(); ?>


<!-- POST FIELDS-->

<li>


	
<?php if ( get_the_post_thumbnail( $post_id ) != '' ) { ?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>  
<div class="entry-content">  
	<div class="archive_left_picture">	
	<a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_post_thumbnail( 'medium' ); ?></a>
	</div><!-- ends left picture -->

<?php if (!empty($post->post_excerpt)) : ?>
		<div class="archive_right_text">
		<header class="entry-header">
		<h1 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>	
		</header><!-- .entry-header -->
<?php the_excerpt();?>

<p class="archive-link"><a href="<?php the_permalink(); ?>" rel="bookmark">read more</a></p>

<?php else : ?>
		<div class="archive_right_text">
		<header class="entry-header">
		<h1 class="entry-title"><?php the_title(); ?></h1>	
		</header><!-- .entry-header -->
<?php the_content(); ?>

<?php endif; ?>
		</div><!-- ends right text -->
	</div>	<!-- ends entry content -->
</article><!-- #post-## -->
<div class="clear"></div>


<?php   } else { ?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>  

	<div class="entry-content">
		<header class="entry-header">
		
		</header><!-- .entry-header -->
<?php if (!empty($post->post_excerpt)) : ?>
<h1 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h1>	
<?php the_excerpt();?>

<p class="archive-link"><a href="<?php the_permalink(); ?>" rel="bookmark">read more</a></p>

<?php else : ?>
<h1 class="entry-title"><?php the_title(); ?></h1>	
<?php the_content(); ?>

<?php endif; ?>
	</div><!-- .entry-content -->
 <?php    } ?>

</article><!-- #post-## -->
<div class="clear"><hr/></div>
</li>
	

	<?php if ( ! post_password_required() && ( comments_open() || '0' != get_comments_number() ) ) : ?>
	
	<?php endif; ?>

<!-- ENDS POST FIELDS -->

			<?php endwhile; ?>
</ul><!-- ends archive array-->
			<?php plasterdog_secondhero_paging_nav(); ?>

		<?php else : ?>

			<?php get_template_part( 'content', 'none' ); ?>

		<?php endif; ?>

		</main><!-- #main -->
	</section><!-- #primary -->

	<div id="secondary" class="widget-area" role="complementary">
		

<?php if ( ! dynamic_sidebar( 'sidebar-1' ) ) : ?>
					
					
					<?php endif; // end sidebar widget area ?>		
									
				
			</header><!-- .page-header -->
		

	</div><!-- #secondary -->

<?php get_footer(); ?>
