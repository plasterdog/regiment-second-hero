<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package regiment-secondhero
 */
?>

	</div><!-- #content -->
</div><!-- #page -->
	<footer id="colophon" class="site-footer" role="contentinfo">
		<div id="foot-constraint">
			<div class="footer-menu clear">
<!-- https://codex.wordpress.org/Function_Reference/has_nav_menu -->
 <?php
if ( has_nav_menu( 'footer' ) ) {
     wp_nav_menu( array( 'theme_location' => 'footer' ) );
} ?> 
			</div>
			<div class="left-side">
			<?php if(get_option('pdog_address') && get_option('pdog_address') != '') {?>
			<span class="icon-text"><?php echo get_option('pdog_address') ?></span> <?php } ?>

			<?php if(get_option('pdog_address2') && get_option('pdog_address2') != '') {?>
			<span class="icon-text"><?php echo get_option('pdog_address2') ?></span>  <?php } ?>

			<?php if(get_option('pdog_phone') && get_option('pdog_phone') != '') {?>
			<span class="icon-text"><?php echo get_option('pdog_phone') ?></span><?php } ?>	

			<?php if(get_option('pdog_phone2') && get_option('pdog_phone2') != '') {?>
			<span class="icon-text"><?php echo get_option('pdog_phone2') ?></span><?php } ?>	

			<?php if(get_option('pdog_email') && get_option('pdog_email') != '') {?>
			<span class="icon-text">	
			<a href="mailto:<?php echo get_option('pdog_email') ?>?subject=Website enquiry" ><?php echo get_option('pdog_email') ?></a></span>	<?php } ?>							


			</div>

			<div class="right-side">

			<div class="social clear">
				<ul class="foot-social-icons">
	

					<?php if(get_option('pdog_facebook') && get_option('pdog_facebook') != '') {?>
					<li><a href="<?php echo get_option('pdog_facebook') ?>" target="_blank"><i class="fab fa-facebook-square"></i></a>	</li><?php } ?>

					<?php if(get_option('pdog_linkedin') && get_option('pdog_linkedin') != '') {?>
					<li><a href="<?php echo get_option('pdog_linkedin') ?>" target="_blank"><i class="fab fa-linkedin"></i></a>	</li><?php } ?>	
			
					<?php if(get_option('pdog_twitter') && get_option('pdog_twitter') != '') {?>
					<li><a href="<?php echo get_option('pdog_twitter') ?>" target="_blank"><i class="fab fa-twitter-square"></i></a>	</li><?php } ?>	
					
					<?php if(get_option('pdog_instagram') && get_option('pdog_instagram') != '') {?>
					<li><a href="<?php echo get_option('pdog_instagram') ?>" target="_blank"><i class="fab fa-instagram"></i></a>	</li><?php } ?>	

										
					<?php if(get_option('pdog_youtube') && get_option('pdog_youtube') != '') {?>
					<li><a href="<?php echo get_option('pdog_youtube') ?>" target="_blank"><i class="fab fa-youtube"></i></a>	</li><?php } ?>	
			
					<?php if(get_option('pdog_vimeo') && get_option('pdog_vimeo') != '') {?>
					<li><a href="<?php echo get_option('pdog_vimeo') ?>" target="_blank"><i class="fab fa-vimeo-square"></i></a>	</li><?php } ?>										
				
					<?php if(get_option('pdog_email') && get_option('pdog_email') != '') {?>
					<li><a href="mailto:<?php echo get_option('pdog_email') ?>?subject=Website enquiry" ><i class="fas fa-envelope"></i></a>	</li><?php } ?>	
										
				</ul>
				</div><!-- ends social-->

			<p class="icon-text"><?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?>
			<br/>&copy;<?php $the_year = date("Y"); echo $the_year; ?> , All rights reserved.
				<?php if(get_option('pdog_target') && get_option('pdog_target') != '') {?></p>
				<p class="icon-text attribution">Site design by <a href="<?php echo get_option('pdog_target') ?>" target="_blank"><?php echo get_option('pdog_attribute') ?></a>
				</p>
				<?php } ?>	
			</p>

			
			</div>
			<div class="home-foot-terms-widget"><?php if ( ! dynamic_sidebar( 'sidebar-4' ) ) : ?><?php endif; // end sidebar widget area ?></div>
			<div class="site-info">
			</div><!-- .site-info -->
		</div>
	</footer><!-- #colophon -->

<?php wp_footer(); ?>

</body>
</html>
