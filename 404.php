<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package regiment-secondhero
 */

get_header(); ?>

<div id="header-bump"></div>
<?php if ( get_field( 'page_secondhero_image' ) ): ?>
<div id="secondhero-top">		
<img src="<?php echo esc_url( get_field( 'page_secondhero_image' ) ); ?>"/>	
<h1><?php the_title(); ?></h1>
</div>
<?php endif; ?>	

<?php if (! get_field( 'page_secondhero_image' ) ): ?>
<div id="big-header-bump" ></div>
<?php endif; ?>	

		<div id="page" class="hfeed site">
	<div id="content" class="site-content" >
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<section class="error-404 not-found">
				<header class="page-header">
					<h1 class="page-title"><?php _e( 'Oops! That page can&rsquo;t be found.', 'regiment-secondhero' ); ?></h1>
				</header><!-- .page-header -->

				<div class="page-content">
					<p><?php _e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'regiment-secondhero' ); ?></p>

					<?php get_search_form(); ?>


					<?php
					/* translators: %1$s: smiley */
					$archive_content = '<p>' . sprintf( __( 'Try looking in the monthly archives. %1$s', 'regiment-secondhero' ), convert_smilies( ':)' ) ) . '</p>';
					the_widget( 'WP_Widget_Archives', 'dropdown=1', "after_title=</h2>$archive_content" );
					?>

					<?php the_widget( 'WP_Widget_Tag_Cloud' ); ?>

				</div><!-- .page-content -->
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->
<?php get_sidebar(); ?>
<?php get_footer(); ?>